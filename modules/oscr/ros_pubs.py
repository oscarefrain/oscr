
from sensor_msgs.msg import JointState
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped
import rospy


class JointStatePub(object):
    """
    Class to publish joint states for display in RViz

    """
    def __init__(self, ndof, fbase, topic='joint_states', buffer=1000):
        self.ndof = ndof
        self.has_floating_base = fbase
        self.jstate_pub = rospy.Publisher(topic, JointState, queue_size=buffer)
        self.jstate = JointState()
        self.jstate.header.stamp = rospy.Time.now()

    def setJointNames(self, jnames):
        if self.has_floating_base:
            floating_base_names = ('Px','Py','Pz','W','Ex','Ey','Ez')
            self.jstate.name = floating_base_names+jnames
            # rospy.logerror('setJointNames with floating base.')
        else:
            self.jstate.name = jnames

    def publish(self, q):
        self.jstate.position = q
        self.jstate.header.stamp = rospy.Time.now()
        self.jstate_pub.publish(self.jstate)


class JointCommandPub(object):
    """
    Class to publish joint commands for Gazebo

    """
    def __init__(self, ndof, fbase, topic='joint_angles', buffer = 1000):
        self.ndof = ndof
        # TODO

    def setJointNames(self, jnames):
        # TODO
        pass

    def publish(self, q):
        # TODO
        pass


class PathPub(object):
    """
    Class to publish the path for display in RViz

    """
    def __init__(self, topic='op_path', buffer=1000):
        """
        Constructor. Important: the frame_id for the message is directly
        obtained from the parameter 'reference_frame', which must be set.

          topic - where the path will be published.
          buffer - buffer size

        """
        self.path_pub = rospy.Publisher(topic, Path, queue_size=buffer)
        self.path = Path()
        frame_id = rospy.get_param('~reference_frame', 'base_link')
        self.path.header.frame_id = frame_id

    def publish(self, pose):
        """
        Add the pose to the path and publish

        """
        self.path.header.stamp = rospy.Time.now()
        # Create a new pose
        self.kpose = PoseStamped()
        self.kpose.header.stamp = rospy.Time.now()
        # New position
        self.kpose.pose.position.x = pose[0,0]
        self.kpose.pose.position.y = pose[1,0]
        self.kpose.pose.position.z = pose[2,0]
        # New orientation
        self.kpose.pose.orientation.x = pose[4,0]
        self.kpose.pose.orientation.y = pose[5,0]
        self.kpose.pose.orientation.z = pose[6,0]
        self.kpose.pose.orientation.w = pose[3,0]
        # Set pose to the path and publish
        self.path.poses.append(self.kpose)
        self.path_pub.publish(self.path)
